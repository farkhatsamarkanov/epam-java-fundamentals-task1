package com.epam.javafundametals;

public class FirstTask
{
    public static void main( String[] args )
    {
        IntegerInputHandler integerInputHandler = new IntegerInputHandler(); // Creating an instance of input handling class
        integerInputHandler.greetUser();                                     //greeting user
        integerInputHandler.printSumAndMultiplicationOfEnteredIntegers();    //processing user's input
    }
}
