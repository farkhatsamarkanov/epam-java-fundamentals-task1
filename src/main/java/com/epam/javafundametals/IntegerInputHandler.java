package com.epam.javafundametals;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class IntegerInputHandler {
    private BufferedReader inputReader = new BufferedReader(new InputStreamReader(System.in));

    public void printSumAndMultiplicationOfEnteredIntegers() {
        System.out.println("Please enter a sequence of integers separated by spaces...");
        boolean repeatInputRequest = true;                                     // implementing continuous app execution
        while (repeatInputRequest) {
            try {
                String typedIntegers = inputReader.readLine();
                String[] arrayOfSeparatedIntegers = typedIntegers.split(" ");
                if (!typedIntegers.equals("")) {                               //checking if input line is empty
                    int sumOfIntegers = 0;
                    int multiplicationOfIntegers = 1;
                    int wrongInputCounter = 0;
                    for (String typedInteger : arrayOfSeparatedIntegers) {     //looping through input data
                        try {                                                  //checking if entered data are integers
                            sumOfIntegers += Integer.parseInt(typedInteger);
                            multiplicationOfIntegers = multiplicationOfIntegers * Integer.parseInt(typedInteger);
                        } catch (NumberFormatException e) {
                            ++wrongInputCounter;                                //if not, increase the counter
                        }
                    }
                    if (wrongInputCounter == arrayOfSeparatedIntegers.length) { //all of entered data are not integers
                        System.out.println("All of entered integers are not integers! Please try again!");
                    } else if (wrongInputCounter > 0) {                        //sum and multiply only integers
                        System.out.println("Some of entered integers are not integers!");
                        System.out.println("Sum of remaining integers: " + sumOfIntegers);
                        System.out.println("Multiplication of remaining integers: " + multiplicationOfIntegers);
                        repeatInputRequest = false;
                    } else {                                                    //all of entered data are integers
                        System.out.println("Sum of entered integers: " + sumOfIntegers);
                        System.out.println("Multiplication of entered integers: " + multiplicationOfIntegers);
                        repeatInputRequest = false;
                    }
                } else {
                    System.out.println("Entered sequence is empty! Please try again!");
                }
            } catch (IOException e) {
                e.printStackTrace();
                break;
            }
        }
        try {
            inputReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void greetUser() {                                                               //method to greet user
        System.out.println("Please enter your name...");
        try {
            String userName = inputReader.readLine();                                        //reading user's name
            System.out.println("Greetings, " + userName + "!");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
